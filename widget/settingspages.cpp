/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*/

/****************************************************************
**
** Implementation of the TrainingPage, DatabasePage and
** OtherPage class
** File name: settingspages.cpp
**
****************************************************************/

#include <QColor>
#include <QColorDialog>
#include <QCoreApplication>
#include <QFileDialog>
#include <QFontDialog>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPalette>
#include <QSettings>
#include <QSqlQuery>
#include <QVBoxLayout>
#include <QVariant>

#include "def/defines.h"
#include "def/errordefines.h"
#include "errormessage.h"
#include "regexpdialog.h"
#include "settingspages.h"
#include "sql/startsql.h"

TrainingPage::TrainingPage(QWidget* parent)
    : QWidget(parent)
{
    // Default ticker font if there is no saved ticker font
    tickerFont = QFont(FONT_STANDARD, FONT_SIZE_TICKER);

    // Create group boxes with settings
    createGroupTickerFont();
    // Create group with sound output settings
    createGroupSoundOutput();

    // Set the layout of all widgets created above
    createLayout();

    // Widget connections
    connect(buttonSetFont, SIGNAL(clicked()), this, SLOT(setFont()));
    connect(buttonSetFontColor, SIGNAL(clicked()), this, SLOT(setFontColor()));
    connect(buttonSetBgColor, SIGNAL(clicked()), this, SLOT(setBgColor()));
    connect(
        buttonSetCursorColor, SIGNAL(clicked()), this, SLOT(setCursorColor()));
    connect(checkMetronome, SIGNAL(toggled(bool)), spinMetronomeClock,
        SLOT(setEnabled(bool)));

    // Read settings
    readSettings();
}

void TrainingPage::createGroupTickerFont()
{
    // Group "Ticker"
    groupTickerFont = new QGroupBox(tr("Ticker"));

    // Button "Set ticker font"
    labelTickerFont = new QLabel(tr("Font:"));
    buttonSetFont = new QPushButton(tr("Change &Font"));
    buttonSetFont->setToolTip(
        tr("Here you can change the font of the ticker\n"
           "(a font size larger than 20 pt is not recommended)"));

    // Button "Font color"
    labelTickerFontColor = new QLabel(tr("Font Color:"));
    buttonSetFontColor = new QPushButton("");
    buttonSetFontColor->setToolTip(tr("Here you can select the font color"));

    // Button "Background color"
    labelTickerBgColor = new QLabel(tr("Background:"));
    buttonSetBgColor = new QPushButton("");
    buttonSetBgColor->setToolTip(
        tr("Here you can select the background color"));

    // Button "Cursor color"
    labelTickerCursorColor = new QLabel(tr("Cursor:"));
    buttonSetCursorColor = new QPushButton("");
    buttonSetCursorColor->setToolTip(tr(
        "Here can select the color of the cursor for the current character"));

    // Spinbox "Set ticker speed"
    labelTickerSpeed = new QLabel(tr("Speed:"));
    labelTickerSpeedMax = new QLabel(tr("Fast"));
    labelTickerSpeedMin = new QLabel(tr("Off"));
    sliderTickerSpeed = new QSlider(Qt::Horizontal);
    sliderTickerSpeed->setMinimum(0);
    sliderTickerSpeed->setMaximum(4);
    sliderTickerSpeed->setToolTip(
        tr("Here you can change the speed of the ticker\n"
           "(Slider on the left: Ticker does not move until reaching the end "
           "of line.\n"
           "Slider on the right: The ticker moves very fast.)"));

    // Layout of group box vertical
    QHBoxLayout* sliderlayout = new QHBoxLayout;
    sliderlayout->addWidget(labelTickerSpeedMin);
    sliderlayout->addSpacing(2);
    sliderlayout->addWidget(sliderTickerSpeed);
    sliderlayout->addSpacing(2);
    sliderlayout->addWidget(labelTickerSpeedMax);

    QVBoxLayout* fontlayout = new QVBoxLayout;
    fontlayout->addWidget(labelTickerFont);
    fontlayout->addWidget(buttonSetFont);

    QVBoxLayout* fontColorlayout = new QVBoxLayout;
    fontColorlayout->addWidget(labelTickerFontColor);
    fontColorlayout->addWidget(buttonSetFontColor);

    QVBoxLayout* bgColorlayout = new QVBoxLayout;
    bgColorlayout->addWidget(labelTickerBgColor);
    bgColorlayout->addWidget(buttonSetBgColor);

    QVBoxLayout* cursorColorlayout = new QVBoxLayout;
    cursorColorlayout->addWidget(labelTickerCursorColor);
    cursorColorlayout->addWidget(buttonSetCursorColor);

    QHBoxLayout* colorLayout = new QHBoxLayout;
    colorLayout->addLayout(fontlayout);
    colorLayout->addLayout(fontColorlayout);
    colorLayout->addLayout(bgColorlayout);
    colorLayout->addLayout(cursorColorlayout);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addLayout(colorLayout);
    layout->addSpacing(10);
    layout->addWidget(labelTickerSpeed);
    layout->addLayout(sliderlayout);
    // layout->addStretch(1);
    layout->setMargin(16);
    groupTickerFont->setLayout(layout);
}

void TrainingPage::createGroupSoundOutput()
{
    // Group "Ticker"
    groupSoundOutput = new QGroupBox(tr("Audio Output"));

    checkMetronome = new QCheckBox(tr("Metronome:"));
    checkMetronome->setToolTip(tr("Here you can activate a metronome"));

    spinMetronomeClock = new QSpinBox();
    spinMetronomeClock->setMinimum(1);
    spinMetronomeClock->setMaximum(200);
    spinMetronomeClock->setSingleStep(1);
    spinMetronomeClock->setSuffix(tr(" cpm"));
    spinMetronomeClock->setToolTip(tr("Please select how often the metronome "
                                      "sound should appear per minute"));

    // Layout of group box vertical
    QHBoxLayout* metronome = new QHBoxLayout;
    metronome->addWidget(checkMetronome);
    metronome->addWidget(spinMetronomeClock);
    metronome->addStretch(1);
    // Layout of group box vertical
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addLayout(metronome);
    layout->setMargin(16);
    groupSoundOutput->setLayout(layout);
}

void TrainingPage::createLayout()
{
    // Group layout vertical
    QVBoxLayout* boxesLayout = new QVBoxLayout;
    boxesLayout->addWidget(groupTickerFont);
    // boxesLayout->addWidget(groupKeyboardLayout);
    boxesLayout->addWidget(groupSoundOutput);
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(boxesLayout);
    // mainLayout->setMargin(5);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void TrainingPage::setFont()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, tickerFont, this);
    if (ok) {
        tickerFont = font;
        setFontButtonLabel();
    }
}

void TrainingPage::setFontColor()
{
    QColor color = QColorDialog::getColor(
        buttonSetFontColor->palette().window().color(), this);
    if (color.isValid()) {
        buttonSetFontColor->setPalette(QPalette(color));
    }
}

void TrainingPage::setBgColor()
{
    QColor color = QColorDialog::getColor(
        buttonSetBgColor->palette().window().color(), this);
    if (color.isValid()) {
        buttonSetBgColor->setPalette(QPalette(color));
    }
}

void TrainingPage::setCursorColor()
{
    QColor color = QColorDialog::getColor(
        buttonSetCursorColor->palette().window().color(), this);
    if (color.isValid()) {
        buttonSetCursorColor->setPalette(QPalette(color));
    }
}

void TrainingPage::setFontButtonLabel()
{
    buttonSetFont->setText(
        (tickerFont.family().length() > 8 ? tickerFont.family().left(6)
                                          : tickerFont.family())
        + (tickerFont.family().length() > 8 ? "..." : "") + ", "
        + QString::number(tickerFont.pointSize()));
}

void TrainingPage::readSettings()
{
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("settings");
    buttonSetFontColor->setPalette(QPalette(
        QColor(settings.value("ticker_font_color", QString(TICKER_COLOR_FONT))
                   .toString())));
    buttonSetBgColor->setPalette(QPalette(
        QColor(settings.value("ticker_bg_color", QString(TICKER_COLOR_BG))
                   .toString())));
    buttonSetCursorColor->setPalette(QPalette(QColor(
        settings.value("ticker_cursor_color", QString(TICKER_COLOR_CURSOR))
            .toString())));
    tickerFont.fromString(
        settings.value("ticker_font", tickerFont.toString()).toString());
    sliderTickerSpeed->setValue(
        settings.value("ticker_speed", TICKERSPEED_STANDARD).toInt());
    settings.endGroup();
    settings.beginGroup("sound");
    checkMetronome->setChecked(
        settings.value("check_metronome", false).toBool());
    spinMetronomeClock->setValue(
        settings.value("spin_metronome", METRONOM_STANDARD).toInt());
    spinMetronomeClock->setEnabled(checkMetronome->isChecked());
    settings.endGroup();
    setFontButtonLabel();
}

void TrainingPage::writeSettings()
{
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("settings");
    settings.setValue("ticker_font_color",
        buttonSetFontColor->palette().window().color().name());
    settings.setValue(
        "ticker_bg_color", buttonSetBgColor->palette().window().color().name());
    settings.setValue("ticker_cursor_color",
        buttonSetCursorColor->palette().window().color().name());
    settings.setValue("ticker_font", tickerFont.toString());
    settings.setValue("ticker_speed", sliderTickerSpeed->value());
    settings.endGroup();
    settings.beginGroup("sound");
    settings.setValue("check_metronome", checkMetronome->isChecked());
    settings.setValue("spin_metronome", spinMetronomeClock->value());
    settings.endGroup();
}

//--------------------------------------------------------

DatabasePage::DatabasePage(QWidget* parent)
    : QWidget(parent)
{
    // Create group boxes with settings
    createGroupUserReset();

    // Set the layout of all widgets created above
    createLayout();

    // Widget connections
    connect(buttonLessonsReset, SIGNAL(clicked()), this,
        SLOT(deleteUserLessonList()));
    connect(buttonCharsReset, SIGNAL(clicked()), this, SLOT(deleteUserChars()));
}

void DatabasePage::createGroupUserReset()
{
    // Group "Keyboard layout"
    groupUserReset = new QGroupBox(tr("User Data"));

    // Button "Reset user data"
    buttonLessonsReset = new QPushButton(tr("Reset &completed lessons"));
    buttonLessonsReset->setToolTip(
        tr("Here you can reset all saved lesson data (the lessons will be "
           "empty as they were after initial installation)"));

    // Button "Reset user data"
    buttonCharsReset = new QPushButton(tr("Reset all &recorded characters"));
    buttonCharsReset->setToolTip(tr(
        "Here you can reset all recorded keystrokes and typing mistakes (the "
        "characters will be empty as they were after initial installation)"));

    // Layout of group box
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(buttonLessonsReset);
    layout->addWidget(buttonCharsReset);
    // layout->addStretch(1);
    layout->setMargin(16);
    groupUserReset->setLayout(layout);
}

void DatabasePage::createLayout()
{
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(groupUserReset);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void DatabasePage::deleteUserLessonList()
{
    QSqlQuery query;
    switch (QMessageBox::question(this, APP_NAME,
        tr("All data for completed lessons for the current user will be "
           "deleted\nand the lesson list will return to its original state "
           "after initial installation!\n\nDo you really want to "
           "continue?\n\n"),
        tr("&Yes"), tr("&Cancel"), nullptr, 1)) {
    case 0:
        StartSql* userSql = new StartSql();
        if (!userSql->deleteUserLessonList()) {
            // Error message
            ErrorMessage* errorMessage = new ErrorMessage(this);
            errorMessage->showMessage(ERR_USER_LESSONS_FLUSH,
                ErrorMessage::Type::Warning, ErrorMessage::Cancel::Operation);
            return;
        }
        QMessageBox::information(this, APP_NAME,
            tr("The data for completed lessons was successfully deleted!\n"));
        break;
    }
}

void DatabasePage::deleteUserChars()
{

    switch (QMessageBox::question(this, APP_NAME,
        tr("All recorded characters (mistake quotas) of the current user will "
           "be deleted and the character list will return to its original "
           "state after initial installation!\n\nDo you really want to "
           "continue?"),
        tr("&Yes"), tr("&Cancel"), nullptr, 1)) {
    case 0:
        StartSql* userSql = new StartSql();
        if (!userSql->deleteUserChars()) {
            // Error message
            ErrorMessage* errorMessage = new ErrorMessage(this);
            errorMessage->showMessage(ERR_USER_ERRORS_FLUSH,
                ErrorMessage::Type::Warning, ErrorMessage::Cancel::Operation);
            return;
        }
        QMessageBox::information(this, APP_NAME,
            tr("The recorded characters were successfully deleted!\n"));
        break;
    }
}

//--------------------------------------------------------

OtherPage::OtherPage(QWidget* parent)
    : QWidget(parent)
{

    // Create group with info window settings
    createGroupDialogCheck();

    // Create group with adaptation settings
    createGroupAdaptation();

    // Set the layout of all widgets created above
    createLayout();

    // Read settings
    readSettings();
}

void OtherPage::createGroupDialogCheck()
{
    // Group "Ticker"
    groupDialogCheck = new QGroupBox(tr("Windows"));

    // Check box "Show start window"
    checkIllustration = new QCheckBox(tr("Show welcome message at startup"));
    checkIllustration->setToolTip(
        tr("Here you can decide if an information window with tips\n"
           "is shown at the beginning of %1")
            .arg(APP_NAME));

    // Check box "Show start window"
    checkTxtMessage
        = new QCheckBox(tr("Notice prior to the import and export of lessons"));
    checkTxtMessage->setToolTip(
        tr("Here you can determine whether a notice to\n"
           "the file types should be displayed before the\n"
           "import and export"));

    // Check box "Show intelligence warning"
    checkIntelligence
        = new QCheckBox(tr("Remember enabled intelligence before starting\n"
                           "an open or own lesson"));
    checkIntelligence->setToolTip(
        tr("Here you can define whether a warning window\n"
           "is displayed when an open or own lesson with\n"
           "active intelligence is started"));

    // Check box "Intelligence duration toggling"
    checkLimitLesson = new QCheckBox(
        tr("Change duration of the lesson automatically to\n"
           "\"Entire lesson\" when disabling the intelligence"));

    // Layout of group box vertical
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(checkIllustration);
    layout->addSpacing(8);
    layout->addWidget(checkIntelligence);
    layout->addSpacing(8);
    layout->addWidget(checkLimitLesson);
    layout->setMargin(16);
    groupDialogCheck->setLayout(layout);
}

void OtherPage::createGroupAdaptation()
{
    // Group "Ticker"
    groupAdaptation = new QGroupBox(tr("Other"));

    checkNativeStyle = new QCheckBox(tr("Use native user interface style"));

    // Layout of group box vertical
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(checkNativeStyle);
    layout->setMargin(16);
    groupAdaptation->setLayout(layout);
}

void OtherPage::createLayout()
{
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(groupDialogCheck);
    mainLayout->addWidget(groupAdaptation);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void OtherPage::readSettings()
{
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    checkIllustration->setChecked(
        settings.value("check_illustration", true).toBool());
    checkIntelligence->setChecked(
        settings.value("check_toggle_intelligence", true).toBool());
    checkLimitLesson->setChecked(
        settings.value("check_limit_lesson", true).toBool());
    checkNativeStyle->setChecked(
        settings.value("check_native_style", false).toBool());
    settings.endGroup();
}

bool OtherPage::writeSettings()
{

    bool requireRestart = false;

#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    if (checkNativeStyle->isChecked()
        != settings.value("check_native_style", false).toBool()) {

        requireRestart = true;
    }
    settings.setValue("check_illustration", checkIllustration->isChecked());
    settings.setValue(
        "check_toggle_intelligence", checkIntelligence->isChecked());
    settings.setValue("check_limit_lesson", checkLimitLesson->isChecked());
    settings.setValue("check_native_style", checkNativeStyle->isChecked());
    settings.endGroup();

    return requireRestart;
}

//--------------------------------------------------------

LanguagePage::LanguagePage(QWidget* parent)
    : QWidget(parent)
{

    // Create group with language settings
    createGroupLanguage();

    // Set the layout of all widgets created above
    createLayout();

    // Read settings
    readSettings();

    checkLessonToLayout();

    connect(comboLayouts, SIGNAL(currentIndexChanged(int)), this,
        SLOT(checkLessonToLayout()));
    connect(comboLayouts, SIGNAL(currentIndexChanged(int)), this,
        SLOT(clearLayoutSetting()));
    connect(comboLessons, SIGNAL(currentIndexChanged(int)), this,
        SLOT(checkLessonToLayout()));
    connect(
        buttonLayoutRegEx, SIGNAL(clicked()), this, SLOT(showLayoutAdvanced()));
}

void LanguagePage::createGroupLanguage()
{

    StartSql* startSql = new StartSql();

    groupLanguage = new QGroupBox(tr("Language"));

    labelLayout = new QLabel(tr("Keyboard Layout:"));

    comboLayouts = new QComboBox();

    startSql->fillLanguage(comboLayouts, "language_layouts", "layout");

    buttonLayoutRegEx = new QPushButton(tr("Advanced"));
    buttonLayoutRegEx->setFixedHeight(20);

    labelLesson = new QLabel(tr("Training Lessons:"));

    comboLessons = new QComboBox();

    startSql->fillLanguage(comboLessons, "language_lessons", "lesson");

    labelLessonNotice = new QLabel();
    QFont h2;
#ifdef APP_MAC
    h2.setPointSize(11);
#else
    h2.setPointSize(7);
#endif
    labelLessonNotice->setWordWrap(true);
    labelLessonNotice->setFont(h2);
    labelLessonNotice->setText(
        tr("The training lessons you have chosen are not suited for the "
           "keyboard layout. You can continue but you may have to put aside "
           "some keys from the beginning."));

    QHBoxLayout* layoutRegexp = new QHBoxLayout;
    layoutRegexp->addWidget(comboLayouts);
    layoutRegexp->addWidget(buttonLayoutRegEx);
    // Layout of group box
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(labelLayout);
    layout->addLayout(layoutRegexp);
    layout->addSpacing(12);
    layout->addWidget(labelLesson);
    layout->addWidget(comboLessons);
    layout->addSpacing(10);
    layout->addWidget(labelLessonNotice);
    // layout->addSpacing(20);
    layout->addStretch(1);
    layout->setMargin(16);
    groupLanguage->setLayout(layout);
}

void LanguagePage::createLayout()
{
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(groupLanguage);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void LanguagePage::checkLessonToLayout()
{
    QString layout
        = comboLayouts->itemData(comboLayouts->currentIndex()).toString();
    QString lessons
        = comboLessons->itemData(comboLessons->currentIndex()).toString();

    if (layout.contains(lessons.mid(3))
        || (lessons.mid(3) == "de_qwertz" && layout.contains("ch_qwertz"))) {
        labelLessonNotice->setVisible(false);
    } else {
        labelLessonNotice->setVisible(true);
    }
}

void LanguagePage::clearLayoutSetting()
{
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    settings.setValue("layout_replace", "NULL");
    settings.setValue("layout_regexp", "NULL");
    settings.endGroup();
}

void LanguagePage::showLayoutAdvanced()
{
    RegExpDialog regExpDialog(
        comboLayouts->itemData(comboLayouts->currentIndex()).toString(), this);
    regExpDialog.exec();
}

void LanguagePage::readSettings()
{
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    int tempIndex = comboLayouts->findData(
        settings.value("language_layout", APP_STD_LANGUAGE_LAYOUT).toString());
    if (!tempIndex) {
        tempIndex = 0;
    }
    comboLayouts->setCurrentIndex(tempIndex);
    tempIndex = comboLessons->findData(
        settings.value("language_lesson", APP_STD_LANGUAGE_LESSON).toString());
    if (!tempIndex) {
        tempIndex = 0;
    }
    comboLessons->setCurrentIndex(tempIndex);
    settings.endGroup();
}

bool LanguagePage::writeSettings()
{

    bool requireRestart = false;

#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    /*if (comboLessons->itemData(comboLessons->currentIndex()) !=
                settings.value("language_lesson",
    APP_STD_LANGUAGE_LESSON).toString()) {

                requireRestart = true;
    }*/
    settings.setValue("language_layout",
        comboLayouts->itemData(comboLayouts->currentIndex()));
    settings.setValue("language_lesson",
        comboLessons->itemData(comboLessons->currentIndex()));
    settings.endGroup();

    return requireRestart;
}
