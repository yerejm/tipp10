/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*/

/****************************************************************
**
** Implementation of the IllustrationDialog class
** File name: illustrationdialog.cpp
**
****************************************************************/

#include <QCoreApplication>
#include <QHBoxLayout>
#include <QIcon>
#include <QPalette>
#include <QSettings>
#include <QVBoxLayout>

#include "def/defines.h"
#include "def/errordefines.h"
#include "errormessage.h"
#include "illustrationdialog.h"

IllustrationDialog::IllustrationDialog(QWidget* parent)
    : QDialog(parent)
{

    setWindowFlags(windowFlags() ^ Qt::WindowContextHelpButtonHint);

    setWindowTitle(tr("Introduction"));

    createContent();

    createButtons();

    // Set the layout of all widgets created above
    createLayout();

    buttonStart->setFocus();

    resize(570, 400);
}

void IllustrationDialog::createContent()
{

    QString content;

    content = ""
              "<div "
              "style=\"margin-top:12px;margin-left:12px;margin-bottom:12px;"
              "margin-right:12px;font-size:12px;\">"
              "<div style=\"margin-top:16px;\">"
              "<center>"
              "&nbsp;<br><img src=\":/img/logo_orange.png\">"
              "</center>"
              "</div>"
              "<div style=\"margin-top:20px;\">"
              "<center>"
              "<b>"
        + tr("Welcome to TIPP10")
        + "</b>"
          "</center>"
          "</div>"
          "<div style=\"margin-top:16px;\">"
        + tr("TIPP10 is a free touch typing tutor for Windows, Mac OS and "
             "Linux. The ingenious thing about the software is its "
             "intelligence feature. Characters that are mistyped are repeated "
             "more frequently. Touch typing has never been so easy to learn.")
        + "</div>"
          "<div style=\"margin-top:17px;\">"
          "<b>"
        + tr("Tips for using the 10 finger system")
        + "</b>"
          "</div>"
          "<div style=\"margin-top:10px;\">"
        + tr("1. First place your fingers in the home position (this is "
             "displayed at the beginning of each lesson). The fingers return "
             "to the home row after each key is pressed.")
        + "</div>"
          "<div style=\"margin-top:16px;\">"
          "<img src=\":/img/"
        + tr("en")
        + "_illustration.png\">"
          "</div>"
          "<div style=\"margin-top:16px;\">"
        + tr("2. Make sure your posture is straight and avoid looking at the "
             "keyboard. Your eyes should be directed toward the monitor at "
             "all times.")
        + "</div>"
          "<div style=\"margin-top:10px;\">"
        + tr("3. Bring your arms to the side of your body and relax your "
             "shoulders. Your upper arm and lower arm should be at a right "
             "angle. Do not rest your wrists and remain in an upright "
             "position.")
        + "</div>"
          "<div style=\"margin-top:10px;\">"
        + tr("4. Try to remain relaxed during the typing lessons.")
        + "</div>"
          "<div style=\"margin-top:10px;\">"
        + tr("5. Try to keep typing errors to a minimum. It is much less "
             "efficient to type fast if you are making a lot of mistakes.")
        + "</div>"
          "<div style=\"margin-top:10px;\">"
        + tr("6. Once you have begun touch typing you have to avoid reverting "
             "back to the way you used to type (even if you are in a hurry).")
        + "</div>"
          "<div style=\"margin-top:26px;\">"
        + tr("If you need assistance with using the software use the help "
             "function.")
        + "</div>"
          "<div style=\"margin-top:34px;\">"
          "<img src=\":/img/tt_logo.png\">"
          "</div>"
          "<div style=\"margin-top:10px;\">"
          "&copy; Tom Thielicke IT Solutions<br>"
          "<a "
          "href=\"http://www.thielicke.org\">http://www.thielicke.org</"
          "a><br><br>"
        + tr("All rights reserved.")
        + "<br>&nbsp;"
          "</div>"
          "</div>";

    illustrationContent = new QTextBrowser();
    illustrationContent->setHtml(content);
    illustrationContent->setOpenExternalLinks(true);
}

void IllustrationDialog::createButtons()
{
    // Buttons
    buttonStart = new QPushButton(this);
    buttonStart->setText(tr("&Launch TIPP10"));
    buttonStart->setDefault(true);
    // Widget connections
    connect(buttonStart, SIGNAL(clicked()), this, SLOT(clickStart()));
    showDialogCheck = new QCheckBox(tr("Do&n't show me this window again"));
}

void IllustrationDialog::createLayout()
{
    // Button layout horizontal
    QHBoxLayout* layoutHorizontal = new QHBoxLayout;
    layoutHorizontal->addSpacing(1);
    layoutHorizontal->addWidget(illustrationContent, 1);
    layoutHorizontal->addSpacing(1);
    // Button layout horizontal
    QHBoxLayout* buttonLayoutHorizontal = new QHBoxLayout;
    buttonLayoutHorizontal->addStretch(1);
    buttonLayoutHorizontal->addWidget(showDialogCheck);
    buttonLayoutHorizontal->addSpacing(8);
    buttonLayoutHorizontal->addWidget(buttonStart);
    // Full layout of all widgets vertical
    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addLayout(layoutHorizontal);
    mainLayout->addSpacing(1);
    mainLayout->addLayout(buttonLayoutHorizontal);
    mainLayout->setMargin(15);
    mainLayout->setSpacing(15);
    // Pass layout to parent widget (this)
    this->setLayout(mainLayout);
}

void IllustrationDialog::clickStart()
{
    writeSettings();
    accept();
}

void IllustrationDialog::writeSettings()
{
// Saves settings of the widget
// (uses the default constructor of QSettings, passing
// the application and company name see main function)
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup("main");
    settings.setValue("check_illustration", !showDialogCheck->isChecked());
    settings.endGroup();
}
