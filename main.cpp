/*
Copyright (c) 2006-2011, Tom Thielicke IT Solutions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*/

/****************************************************************
**
** File name: main.cpp
**
****************************************************************/

#include <QApplication>
#include <QCoreApplication>
#include <QLibraryInfo>
#include <QSettings>
#include <QString>
#include <QTranslator>

#include "def/defines.h"
#include "sql/connection.h"
#include "widget/illustrationdialog.h"
#include "widget/mainwindow.h"

int main(int argc, char* argv[])
{

    QApplication app(argc, argv);

    // Set application name and domain
    // (this saves having to repeat the information
    // each time a QSettings object is created)
    QApplication::setOrganizationName(APP_NAME_INTERN);
    QApplication::setOrganizationDomain(APP_URL);
    QApplication::setApplicationName(APP_NAME_INTERN);
    QApplication::setWindowIcon(QIcon(":/img/icon.svg"));

    // Translation
    // Common qt widgets
    QTranslator translatorQt;
    translatorQt.load("qt_" + QLocale::system().name(),
        QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    QApplication::installTranslator(&translatorQt);

    // Content (own translation files)
    QString trFile = "tipp10_" + QLocale::system().name();
    QString trPath = TIPP10_ROOTDIR + "translations";
    QTranslator translatorContent;
    translatorContent.load(trFile, trPath);
    QApplication::installTranslator(&translatorContent);

// Settings to get and set general settings
#if APP_PORTABLE
    QSettings settings(
        QCoreApplication::applicationDirPath() + "/portable/settings.ini",
        QSettings::IniFormat);
#else
    QSettings settings;

    // move settings to new location
    QSettings oldSettings("Tom Thielicke IT Solutions", APP_NAME_INTERN);
    QFile oldconfig(oldSettings.fileName());
    if (oldconfig.exists())
    {
        QDir oldDir = QFileInfo(oldconfig).dir();

        QFile newconfig(settings.fileName());
        QDir dir = QFileInfo(newconfig).dir();
        dir.mkpath(dir.path());

        oldconfig.rename(settings.fileName());
        oldDir.rmdir(oldDir.path());
        settings.sync();
    }
#endif

    // Read/write language, license key and show illustration flag
    settings.beginGroup("main");
    QString languageLayout = settings.value("language_layout", "").toString();

    QString languageLesson = settings.value("language_lesson", "").toString();

    bool showIllustration = settings.value("check_illustration", true).toBool();
    bool useNativeStyle = settings.value("check_native_style", false).toBool();
    settings.endGroup();

    if (languageLayout == "") {
        if (QObject::tr("en") == "de") {
            languageLesson = "de_de_qwertz";
            languageLayout = "de_qwertz";
        } else {
            languageLesson = "en_us_qwerty";
            languageLayout = "us_qwerty";
        }
#ifdef APP_MAC
        languageLayout.append("_mac");
#else
        languageLayout.append("_win");
#endif
    }

    settings.beginGroup("main");
    settings.setValue("language_layout", languageLayout);
    settings.setValue("language_lesson", languageLesson);
    settings.endGroup();

    // Set windows style
    if (!useNativeStyle) {
        QApplication::setStyle("plastique");
    }

    // Create database connection
    if (!createConnection()) {
        // Cannot find or open database
        // -> exit program
        return 1;
    }

    // Show illustration widget at the beginning if not disabled by the user
    if (showIllustration) {
        IllustrationDialog illustrationDialog(nullptr);
        illustrationDialog.exec();
    }

    // Create main window object
    MainWindow window;
    window.show();

    // Start the event loop
    return QApplication::exec();
}
